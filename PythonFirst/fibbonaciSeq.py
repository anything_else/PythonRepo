from safeCast import *

class fib:
    def _sumLastTwoInt(self):
        lastIndex = len(self.res)-1
        if lastIndex == 0:
            return self.res[lastIndex]
        
        return self.res[lastIndex] + self.res[lastIndex-1]

    def getIntSafe(self,txt):
        res = safeInt(input(txt))
        if res < 0:
            res = self.getIntSafe(txt)
        return res

    def buildFibonaci(self):
        howMany = self.getIntSafe('how long seq: ')
        root = self.getIntSafe('start at: ')
        self.res = [root]
        for i in range(1, howMany):
            self.res.append(self._sumLastTwoInt())   

        print(self.res)         