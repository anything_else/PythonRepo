from safeCast import *

class oddOrEven:
    """Test if input is odd/even"""
    def _getInt(self):
        userInt = safeInt(input('Enter int:'))
        if userInt == -1:
            userInt = self._getInt()
        return userInt
    
    def testIfOdd(self):
        no = self._getInt()
        result = 'Odd'
        if no % 2 == 0:
            result = 'Even'
        return result
