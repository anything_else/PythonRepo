class mThree:  
    def _init_(self):
        self.one = 0
        self.two = 0
        self.three = 0

    def maxOfThree(self):
        if(self.one > self.two and self.one > self.three):            
            return self.one            
        elif(self.two > self.three and self.two > self.one):
            return self.two
        else:
            return self.three

    def getValues(self):
        self.one = input('Enter one:')
        self.two = input('Enter two:')
        self.three = input('Enter three:')