a = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

def lc():
    return [i for i in a if i %2 == 0]

def filterFirstLastList(lst):
    return [j for i,j in enumerate(lst) if (i == 0 or i == (len(lst)-1))]   