class rpsGame:
    gr = '{0} it\'s your play:\tR[ock]\tP[aper]\tS[cisors] ? '
    gamesPlayed = 0;
    p1Won = 0;
    p2Won = 0;

    def _getPlayerName(self, playerId):
        return input('Enter player {0} name: '.format(playerId))

    def _validPlay(self, play):
        result = False
        if play in ['S', 'R', 'P']:
            result = True
        return result

    def _getValidPlay(self, player):
         play = input(rpsGame.gr.format(player)).upper()
         while not self._validPlay(play):
             play = input(gameRequest.format(player)).upper()
         return play
    
    def _getWinner(self, play1, play2):
        rpsGame.gamesPlayed += 1;

        game = play1 + play2 
        winner = self.p1

        if game in ['SR', 'RP', 'PS']:
            winner = self.p2   
            rpsGame.p2Won += 1
        else:
            rpsGame.p1Won += 1

        return winner

    def handleDraw(self, game):
        if game in ['RR', 'PP', 'SS']:
            print('DRAW! GO AGAIN!')
            return self.game()

    def game(self):
          p1Play = self._getValidPlay(self.p1)
          p2Play = self._getValidPlay(self.p2)
          return self._getWinner(p1Play, p2Play)
        
    def mainLoop(self):

        self.p1 = self._getPlayerName('p1')
        self.p2 = self._getPlayerName('p2')        

        while True:
            
            winner = self.game()
            print('Winner is {0}'.format(winner))
            print('Games: {0} {1} won: {2} {3} won: {4}'.format(rpsGame.gamesPlayed, 
                self.p1, rpsGame.p1Won, self.p2, rpsGame.p2Won))
            if input('Play again {0}? [Y/N]\n'.format(winner)).upper() !=  'Y':
                break                
