import datetime
from safeCast import *

class ageCalc:
    """Calculate when user hits 100 bday"""
    def __init__(self):
        self.userAge = 0
        self.reps = 0
    
    def _getUserAge(self):
        self.userAge = safeInt(input('Enter your age:'))
        if self.userAge < 0:
           self._getUserAge()          

    def _calcHundrBirthDay(self):
        return datetime.date.today().year + (100 - self.userAge)

    def getNoOfReps(self):
          val = safeInt(input('Enter no. of reps:'))
          if val < 0:
           self.getNoOfReps()          
          return val
            

    def calculateHundrBDay(self):
        self._getUserAge()
        return self._calcHundrBirthDay()
    
    
        