from FileHelper import fileHelper
from safeCast import *

class itemsStatistics:
    """Load file and group by category"""
    def __init__(self):
        self.results = dict();
        self.fHelper = fileHelper()  

    def countCategories(self, catIndex, splitChar):
        path = self.fHelper.getPath()
        fileData = self.fHelper.readByLine(path)

        for line in fileData:
            parts = str.split(line,splitChar)
            key = parts[catIndex]
            if key in self.results:
                self.results[key] += 1
            else:
                self.results[key] = 1