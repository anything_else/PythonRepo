"""helper methods for safe casting"""

def safeInt(value):
    return _safe_cast(value, int, -1)

def _safe_cast(val, to_type, default=None):
    try:
        return to_type(val)
    except ValueError:
        return default