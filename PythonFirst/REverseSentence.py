class rs:
    def _getInput(self):
        sent = input('enter sentence: ')
        self.words = sent.split()

    def reverse(self):
        self._getInput()
        return list(reversed(self.words))            