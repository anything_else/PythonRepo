class fileHelper:
    """Helper class for file manipulation"""

    def readFile(self, local):
       with open(local, 'r') as open_read:
        return open_read.read();

    def writeNew(self, local, content):
        with open(local, 'w') as open_override:
            open_override.write(content)
    
    def appendToFile(self, local, content):
        with open(local, 'a') as open_append:
            open_append.write(content)   

    def readByLine(self, local):
        results =[]

        with open(local,'r') as open_read:
            results = open_read.readlines()
        return results
    
    def getPath(self):
        return input('enter target path')   