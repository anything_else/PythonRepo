from safeCast import *
from random import *

class gN:
    guesscounter = 0
    target = 0   

    def playGame():
        while True:
            if gN.target == 0:
                gN.target = randint(1,9)
            guess = gN.askForInput()
            if guess < gN.target:
                gN.missHandler('To low')       
            elif guess > gN.target:
                gN.missHandler('To high')        
            else:
                gN.winHandler()
                con =input('Continue? [Y/N]')
                if con.upper() != 'Y':
                    break;

    def winHandler():
        gN.guesscounter += 1
        print('Well done!')
        print('Attempts: {0}'.format(gN.guesscounter))
        gN.guesscounter = 0
        gN.target = 0
       

    def missHandler(displayText):
         print(displayText)
         gN.guesscounter += 1

    def askForInput():
         ret =  safeInt(input('Enter your guess: '))
         if ret < 0:
            gN.askForInput()
         return ret
        
    