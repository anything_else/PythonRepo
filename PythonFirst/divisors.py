from safeCast import *

class divisors:
    """get dibvisors of provided int"""

    def _getRoot(self):
        no = safeInt(input('enter limit int: '))
        if no == -1:
            no = self._getRoot()
        return no

    def findDividers(self):
        no = self._getRoot()        
        return [i for i in range(1,no+1) if( no % i == 0)]

    def isPrime(self):
        res = True
        div = self.findDividers()
        if len(div) > 2:
            res = False
        return res

    def testPrimeMultiple(self):
        while True:
            print(self.isPrime())
            if input('Continue? [Y/N]').upper() != 'Y':
                break
