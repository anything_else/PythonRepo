from random import *

class commonPart:
    """get common part of two lists"""
    
    def _generateList(self, length):
        list = []
        for i in range(0,length):
            list.append(randrange(10))
        return list
    
    def getCommonPartOfRandomLists(self):        
        l1 = self._generateList(7)
        l2 = self._generateList(11)
        result = []    
        [result.append(z) for z in [i for i in l1 if i in l2] if z not in result]
        return result
