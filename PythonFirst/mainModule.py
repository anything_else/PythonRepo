import MaxOfThree
import FileHelper
from  AgeCalc import ageCalc
from dictionaryPlay import itemsStatistics
from OddOrEven import oddOrEven
from ListLessThan import listLessThan
from divisors import divisors
from commonPart import commonPart
from listCOmprehension import *
from stringList import stringList
from rockPaperScisors import rpsGame
from guessNo import *
from fibbonaciSeq import fib
from remDups import rDup
from REverseSentence import rs
from passwordGen import passGen

pg = passGen()
pg.buildPass()

