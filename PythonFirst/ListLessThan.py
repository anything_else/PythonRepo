from safeCast import *

class listLessThan:
    """test values"""

    def __init__(self):
        self.col =  [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]        
        self.limit = 0

    def _getLimit(self):
        no = safeInt(input('enter limit int: '))
        if no == -1:
            no = self._getLimit()
        return no

    def testValues(self):
        self.limit = self._getLimit()
        resCol = []
        for i in self.col:
            if i < self.limit:
                resCol.append(i)
        return resCol
